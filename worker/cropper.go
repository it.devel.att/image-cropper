package worker

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"image_crop/queue"
	"image_crop/repository"
	"image_crop/service"
	"sync"
	"time"
)

type (
	worker struct {
		cfg               config.Config
		ctx               context.Context
		queue             queue.Queue
		wg                *sync.WaitGroup
		imgCropperService service.ImageCropper
		taskService       service.TaskService
		counterService    service.CounterService
		mapRepo           repository.MapRepository
	}
)

func NewWorker(
	cfg config.Config,
	ctx context.Context,
	queue queue.Queue,
	wg *sync.WaitGroup,
	imgCropperService service.ImageCropper,
	taskService service.TaskService,
	counterService service.CounterService,
	mapRepo repository.MapRepository,
) Worker {
	// TODO Add some UUID for each worker?
	return worker{
		cfg:               cfg,
		ctx:               ctx,
		queue:             queue,
		wg:                wg,
		imgCropperService: imgCropperService,
		taskService:       taskService,
		counterService:    counterService,
		mapRepo:           mapRepo,
	}
}

func (w worker) Run() {
	const logSource = "[worker.Run]"
	w.wg.Add(1)

	log.Infof("%v Run", logSource)
	go func() {
		defer w.wg.Done()

		for {
			select {
			case <-w.ctx.Done():
				log.Infof("%v Receive ctx.Done()", logSource)
				return
			default:
				task := w.queue.Get()
				if task != nil {

					log.Infof("%v Get task %#v", logSource, task)
					err := w.imgCropperService.CropByTask(task)
					if err != nil {
						log.Errorf("%v%v", logSource, err)
						w.queue.Push(task)
					} else {
						// TODO it's not very productive to count on every block processing
						blocksCount, err := w.mapRepo.CountMapBlocks(task.NewMap.ID)
						if err != nil {
							log.Errorf("%v%w", logSource, err)
							w.queue.Push(task)
						} else {
							// TODO Compare it from some map value
							if blocksCount >= 40504 {
								if err := w.mapRepo.FinishProcessing(task.NewMap); err != nil {
									log.Errorf("%v%v", logSource, err)
									w.queue.Push(task)
								} else {
									processedTime := task.NewMap.FinishProcessedAt.Sub(task.NewMap.CreatedAt)
									fmt.Printf("Finished at: %v", processedTime)
									log.Infof("%v Finish processing at: %v", logSource, processedTime)
								}
							}
						}
					}
				}
				time.Sleep(time.Millisecond * 100)
			}
		}
	}()
}

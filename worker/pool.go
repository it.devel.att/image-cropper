package worker

import (
	"context"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"image_crop/queue"
	"image_crop/repository"
	"image_crop/service"
	"sync"
)

type (
	Pool interface {
		Run()
		WaitForFinish()
	}

	workerPool struct {
		cfg               config.Config
		ctx               context.Context
		queue             queue.Queue
		wg                *sync.WaitGroup
		mu                *sync.Mutex
		imgCropperService service.ImageCropper
		taskService       service.TaskService
		counterService    service.CounterService
		mapRepo           repository.MapRepository
	}
)

func NewWorkerPool(
	cfg config.Config,
	ctx context.Context,
	queue queue.Queue,
	imgCropperService service.ImageCropper,
	taskService service.TaskService,
	counterService service.CounterService,
	mapRepo repository.MapRepository,
) Pool {
	return workerPool{
		cfg:               cfg,
		ctx:               ctx,
		queue:             queue,
		wg:                &sync.WaitGroup{},
		mu:                &sync.Mutex{},
		imgCropperService: imgCropperService,
		taskService:       taskService,
		counterService:    counterService,
		mapRepo:           mapRepo,
	}
}

func (w workerPool) Run() {
	const logSource = "[workerPool.Run]"

	log.Infof("%v Start WorkerPool", logSource)
	for i := 0; i < w.cfg.GetWorkerPoolSize(); i++ {
		worker := NewWorker(
			w.cfg,
			w.ctx,
			w.queue,
			w.wg,
			w.imgCropperService,
			w.taskService,
			w.counterService,
			w.mapRepo,
		)
		worker.Run()
	}
}

func (w workerPool) WaitForFinish() {
	const logSource = "[workerPool.WaitForFinish]"

	log.Infof("%v Wait workers finish", logSource)
	w.wg.Wait()
	log.Infof("%v Finish", logSource)
}

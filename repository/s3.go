package repository

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"image_crop/config"
	"io/ioutil"
	"os"
	"time"
)

type (
	FileRepository interface {
		GetFile(key string) (*os.File, error)
		UploadFile(file *os.File, key string) error
		DeleteFile(file *os.File) error
		GetSignURL(key string) (string, error)
	}

	fileRepository struct {
		cfg    config.Config
		client *s3.S3
	}
)

func NewFileRepository(
	cfg config.Config,
	client *s3.S3,
) FileRepository {
	return fileRepository{
		cfg:    cfg,
		client: client,
	}
}

func (f fileRepository) GetFile(key string) (*os.File, error) {
	request, response := f.client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(f.cfg.GetS3BucketName()),
		Key:    aws.String(key),
	})
	err := request.Send()
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	// TODO create new tempfile based on map_id or parent_block_id
	newFile, err := os.Create(key)
	if err != nil {
		return newFile, err
	}
	responseBodyBytes, err := ioutil.ReadAll(response.Body)

	_, err = newFile.Write(responseBodyBytes)
	if err != nil {
		return nil, err
	}
	return newFile, nil
}

func (f fileRepository) UploadFile(file *os.File, key string) error {
	logSource := "[fileRepository.UploadFile]"
	if key == "" {
		key = file.Name()
	}
	_, err := file.Seek(0, 0)
	if err != nil {
		return fmt.Errorf("%v Error: %v", logSource, err)
	}
	_, err = f.client.PutObject(&s3.PutObjectInput{
		Body:   file,
		Bucket: aws.String(f.cfg.GetS3BucketName()),
		Key:    aws.String(key),
	})
	if err != nil {
		return fmt.Errorf("%v Error: %v", logSource, err)
	}
	_, err = file.Seek(0, 0)
	if err != nil {
		return fmt.Errorf("%v Error: %v", logSource, err)
	}
	return nil
}

func (f fileRepository) DeleteFile(file *os.File) error {
	logSource := "[fileRepository.DeleteFile]"

	_, err := f.client.DeleteObject(&s3.DeleteObjectInput{
		Bucket: aws.String(f.cfg.GetS3BucketName()),
		Key:    aws.String(file.Name()),
	})
	if err != nil {
		return fmt.Errorf("%v Error: %v", logSource, err)
	}
	return nil
}

func (f fileRepository) GetSignURL(key string) (string, error) {
	logSource := "[fileRepository.GetFullURL]"

	request, _ := f.client.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(f.cfg.GetS3BucketName()),
		Key:    aws.String(key),
	})

	url, err := request.Presign(time.Hour)
	if err != nil {
		return "", fmt.Errorf("%v Error: %v", logSource, err)
	}

	return url, nil
}

package repository

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	model "image_crop/model/db"
)

type (
	MapRepository interface {
		CreateMap(image string) (*model.Map, error)
		CreateMapBlock(mapBlock *model.MapBlock) error
		GetMap(id string) (*model.Map, error)
		GetMaps() ([]*model.Map, error)
		GetMapBlock(id string) (*model.MapBlock, error)
		GetMapBlocksByMap(existMap *model.Map) ([]*model.MapBlock, error)
		GetChildMapBlocks(mapBlock *model.MapBlock) ([]*model.MapBlock, error)
		CountMapBlocks(mapID string) (int64, error)
		FinishProcessing(processedMap *model.Map) error
	}

	mapRepository struct {
		cfg    config.Config
		client *sqlx.DB
	}
)

func NewMapRepository(
	cfg config.Config,
	client *sqlx.DB,
) MapRepository {
	return mapRepository{
		cfg:    cfg,
		client: client,
	}
}

func (m mapRepository) CreateMap(image string) (*model.Map, error) {
	logSource := "[mapRepository.CreateMap]"
	newMap := &model.Map{}

	tx, err := m.client.Begin()
	if err != nil {
		return newMap, fmt.Errorf("%v Error: %v", logSource, err)
	}

	err = tx.QueryRow(`
		INSERT INTO maps (original_image) 
		VALUES ($1) 
		RETURNING 
			id, 
			original_image, 
			created_at
		`,
		image,
	).Scan(
		&newMap.ID,
		&newMap.OriginalImage,
		&newMap.CreatedAt,
	)
	if err != nil {
		if err := tx.Rollback(); err != nil {
			log.Errorf("%v Error while try to rollback: %v", logSource, err)
		}
		return newMap, fmt.Errorf("%v Error %v", logSource, err)
	} else {
		if err := tx.Commit(); err != nil {
			return newMap, fmt.Errorf("%v Error %v", logSource, err)
		}
	}
	return newMap, nil
}

func (m mapRepository) CreateMapBlock(mapBlock *model.MapBlock) error {
	logSource := "[mapRepository.CreateMapBlock]"

	existMapBlock := &model.MapBlock{}
	err := m.client.Get(
		existMapBlock,
		`
			SELECT * FROM map_blocks 
			WHERE map_id = $1 
			  AND block_order = $2 
			  AND parent_map_block_id = $3
			LIMIT 1
		`,
		mapBlock.MapID,
		mapBlock.BlockOrder,
		mapBlock.ParentMapBlockID,
	)

	if err == sql.ErrNoRows {

		tx, err := m.client.Begin()
		if err != nil {
			return fmt.Errorf("%v Error: %v", logSource, err)
		}

		err = tx.QueryRow(`
		INSERT INTO map_blocks 
		    (
				map_id,
				image,
				zoom_level,
		     	block_order,
		     	parent_map_block_id
			) 
		VALUES ($1, $2, $3, $4, $5) 
		RETURNING 
			id,
			created_at
		`,
			mapBlock.MapID,
			mapBlock.Image,
			mapBlock.ZoomLevel,
			mapBlock.BlockOrder,
			mapBlock.ParentMapBlockID,
		).Scan(
			&mapBlock.ID,
			&mapBlock.CreatedAt,
		)
		if err != nil {
			if err := tx.Rollback(); err != nil {
				log.Errorf("%v Error while try to rollback: %v", logSource, err)
			}
			return fmt.Errorf("%v Error %v", logSource, err)
		} else {
			if err := tx.Commit(); err != nil {
				return fmt.Errorf("%v Error %v", logSource, err)
			}
		}
	} else if err != nil {
		//fmt.Printf("mapBlock.MapID %v\n", mapBlock.MapID)
		//fmt.Printf("mapBlock.MapID %v\n", mapBlock.BlockOrder)
		//fmt.Printf("mapBlock.MapID %v\n", mapBlock.ParentMapBlockID)
		return fmt.Errorf("%v Error %v", logSource, err)
	}
	mapBlock = existMapBlock
	return nil
}

func (m mapRepository) GetMapBlocksByMap(existMap *model.Map) ([]*model.MapBlock, error) {
	logSource := "[mapRepository.GetMapBlocksByMap]"

	var mapBlocks []*model.MapBlock
	err := m.client.Select(
		&mapBlocks,
		`
			SELECT * FROM map_blocks
			WHERE map_id = $1 AND zoom_level = 1
			ORDER BY block_order
		`,
		existMap.ID,
	)
	if err != nil {
		return nil, fmt.Errorf("%v Error: %v", logSource, err)
	}
	return mapBlocks, nil
}

func (m mapRepository) GetChildMapBlocks(mapBlock *model.MapBlock) ([]*model.MapBlock, error) {
	logSource := "[mapRepository.GetChildMapBlocks]"

	var mapBlocks []*model.MapBlock
	err := m.client.Select(
		&mapBlocks,
		`
			SELECT * FROM map_blocks 
			WHERE parent_map_block_id = $1
			ORDER BY block_order
		`,
		mapBlock.ID,
	)
	if err != nil {
		return nil, fmt.Errorf("%v Error %v", logSource, err)
	}
	return mapBlocks, nil
}

func (m mapRepository) GetMaps() ([]*model.Map, error) {
	logSource := "[mapRepository.GetMaps]"

	var maps []*model.Map
	err := m.client.Select(
		&maps,
		`SELECT * FROM maps`,
	)
	if err != nil {
		return nil, fmt.Errorf("%v Error %v", logSource, err)
	}
	return maps, nil
}

func (m mapRepository) GetMap(id string) (*model.Map, error) {
	logSource := "[mapRepository.GetMap]"

	existMap := &model.Map{}
	err := m.client.Get(
		existMap,
		`
			SELECT * FROM maps
			WHERE id = $1 LIMIT 1
		`,
		id,
	)
	if err != nil {
		return nil, fmt.Errorf("%v Error: %v", logSource, err)
	}
	return existMap, nil
}

func (m mapRepository) GetMapBlock(id string) (*model.MapBlock, error) {
	logSource := "[mapRepository.GetMapBlock]"

	mapBlocks := &model.MapBlock{}
	err := m.client.Get(
		mapBlocks,
		`
			SELECT * FROM map_blocks 
			WHERE id = $1
			LIMIT 1
		`,
		id,
	)
	if err != nil {
		return nil, fmt.Errorf("%v Error %v", logSource, err)
	}
	return mapBlocks, nil
}

func (m mapRepository) CountMapBlocks(mapID string) (int64, error) {
	logSource := "[mapRepository.CountMapBlocks]"

	var countBlocks int64
	err := m.client.QueryRow(
		`
			SELECT COUNT(id)
			FROM map_blocks
			WHERE map_blocks.map_id = $1
		`,
		mapID,
	).Scan(&countBlocks)
	if err != nil {
		return countBlocks, fmt.Errorf("%v Error: %v", logSource, err)
	}
	return countBlocks, nil
}

func (m mapRepository) FinishProcessing(processedMap *model.Map) error {
	logSource := "[mapRepository.SetFinishProcessingAt]"

	log.Infof("processedMap.ID %v", processedMap.ID)
	err := m.client.QueryRow(
		`
			UPDATE maps
			SET finish_processed_at = NOW()
			WHERE id = $1
			RETURNING finish_processed_at;
			`,
		processedMap.ID,
	).Scan(&processedMap.FinishProcessedAt)
	if err != nil {
		return fmt.Errorf("%v %w", logSource, err)
	}
	return nil
}

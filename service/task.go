package service

import (
	"image_crop/config"
	modelQueue "image_crop/model/queue"
	"image_crop/queue"
)

type (
	TaskService interface {
		Run(task *modelQueue.Task) error
	}

	taskService struct {
		cfg   config.Config
		queue queue.Queue
	}
)

func NewTaskService(
	cfg config.Config,
	queue queue.Queue,
) TaskService {
	return taskService{
		cfg:   cfg,
		queue: queue,
	}
}

func (t taskService) Run(task *modelQueue.Task) error {
	const logSource = "[taskService.Run]"

	t.queue.Push(task)
	return nil
}

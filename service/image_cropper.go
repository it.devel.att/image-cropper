package service

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"image"
	"image/jpeg"
	"image_crop/config"
	model "image_crop/model/db"
	modelQueue "image_crop/model/queue"
	"image_crop/repository"
	"io/ioutil"
	"os"
	"path/filepath"
)

type (
	ImageCropper interface {
		CropImage(file *os.File) (*model.Map, error)
		CreateCropTask(
			fileKey string,
			level int,
			newMap *model.Map,
			parentBlock *model.MapBlock,
		) error
		CropByTask(task *modelQueue.Task) error
	}

	imageCropper struct {
		cfg                config.Config
		fileRepo           repository.FileRepository
		mapRepo            repository.MapRepository
		taskService        TaskService
		coordinatesService CoordinatesService
		counterService     CounterService
	}

	SubImager interface {
		SubImage(r image.Rectangle) image.Image
	}
)

var (
	// Ideal for earth_map.jpg earth_map
	// TODO Make this configurable for each image
	stepForLevel = map[int]int{
		1: 2,
		2: 5,
		3: 2,
		4: 10,
	}
)

func NewImageCropper(
	cfg config.Config,
	fileRepo repository.FileRepository,
	mapRepo repository.MapRepository,
	taskService TaskService,
	coordinatesService CoordinatesService,
	counterService CounterService,
) ImageCropper {
	return imageCropper{
		cfg:                cfg,
		fileRepo:           fileRepo,
		mapRepo:            mapRepo,
		taskService:        taskService,
		coordinatesService: coordinatesService,
		counterService:     counterService,
	}
}

func (s imageCropper) CropImage(file *os.File) (*model.Map, error) {
	logSource := "[imageCropper.CropImage]"
	if err := s.fileRepo.UploadFile(file, ""); err != nil {
		return nil, fmt.Errorf("%v%w", logSource, err)
	}

	newMap, err := s.mapRepo.CreateMap(file.Name())
	if err != nil {
		return newMap, fmt.Errorf("%v Error %v", logSource, err)
	}

	tempDir, err := ioutil.TempDir(s.cfg.GetCroppedImageDir(), "")
	if err != nil {
		return newMap, fmt.Errorf("%v Error %v", logSource, err)

	}

	if err := s.CreateCropTask(newMap.OriginalImage, 1, newMap, nil); err != nil {
		return newMap, fmt.Errorf("%v%v", logSource, err)
	}

	if err := file.Close(); err != nil {
		log.Print(err)
	}

	log.Printf("%v TempDir is %v", logSource, tempDir)
	if s.cfg.GetRemoveLocalDirAfterImageCrop() {
		if err := os.RemoveAll(tempDir); err != nil {
			log.Printf("%v Error %v", logSource, err)
		}
	}
	return newMap, nil
}

func (s imageCropper) CreateCropTask(
	fileKey string,
	level int,
	newMap *model.Map,
	parentBlock *model.MapBlock,
) error {
	err := s.taskService.Run(&modelQueue.Task{
		FileKey:     fileKey,
		Level:       level,
		NewMap:      newMap,
		ParentBlock: parentBlock,
	})
	return err
}

func (s imageCropper) CropByTask(task *modelQueue.Task) error {
	logSource := fmt.Sprintf("[imageCropper.CropByTask.%v]", task.Level)

	file, err := s.fileRepo.GetFile(task.FileKey)
	if err != nil {
		return fmt.Errorf("%v%w", logSource, err)
	}

	img, imgConfig, err := s.getImgWithConfig(file)
	if err != nil {
		return fmt.Errorf("%v%v", logSource, err)
	}
	subImg, ok := img.(SubImager)
	if !ok {
		return fmt.Errorf("%v Error while convert img to SubImager", logSource)
	}

	tempDir, err := ioutil.TempDir(s.cfg.GetCroppedImageDir(), "")
	if err != nil {
		return fmt.Errorf("%v Error %v", logSource, err)
	}

	coords := s.coordinatesService.GenerateCoordinates(imgConfig.Width, imgConfig.Height, stepForLevel[task.Level])
	for i, coord := range coords {
		croppedImgFile, err := s.cropImageByRectangle(i, coord, subImg, tempDir)
		if err != nil {
			//s.deleteUploadedFiles(croppedImages)
			return fmt.Errorf("%v Error %v", logSource, err)
		}
		//croppedImages = append(croppedImages, croppedImgFile)
		err = s.fileRepo.UploadFile(croppedImgFile, "")
		if err != nil {
			//s.deleteUploadedFiles(croppedImages)
			return fmt.Errorf("%v%w", logSource, err)
		}

		var parentMapBlockId *string
		if task.ParentBlock != nil {
			parentMapBlockId = &task.ParentBlock.ID
		}
		mapBlock := model.MapBlock{
			MapID:            task.NewMap.ID,
			ParentMapBlockID: parentMapBlockId,
			BlockOrder:       i,
			ZoomLevel:        task.Level,
			Image:            croppedImgFile.Name(),
		}
		if err := s.mapRepo.CreateMapBlock(&mapBlock); err != nil {
			//s.deleteUploadedFiles(croppedImages)
			return fmt.Errorf("%v%v", logSource, err)
		}

		_, err = s.counterService.Up(task.GetUPKey())
		if err != nil {
			return fmt.Errorf("%v Error %v", logSource, err)
		}

		if task.Level+1 < 5 {
			// TODO It's better to generate dirname by filename without extension
			err := s.CreateCropTask(croppedImgFile.Name(), task.Level+1, task.NewMap, &mapBlock)
			if err != nil {
				return fmt.Errorf("%v Error %v", logSource, err)
			}
		}

		if err := croppedImgFile.Close(); err != nil {
			log.Errorf("%v Error while try to close file %v: %v", logSource, file.Name(), err)
		}
		if err := os.Remove(croppedImgFile.Name()); err != nil {
			log.Errorf("%v Error while try to remove file %v: %v", logSource, file.Name(), err)
		}
	}
	if err := file.Close(); err != nil {
		log.Errorf("%v Error while try to close file %v: %v", logSource, file.Name(), err)
	}
	if err := os.Remove(file.Name()); err != nil {
		log.Errorf("%v Error while try to remove file %v: %v", logSource, file.Name(), err)
	}
	return nil
}

func (s imageCropper) cropImageByRectangle(i int, coords image.Rectangle, img SubImager, tempDir string) (*os.File, error) {
	subImg := img.SubImage(coords)
	imageName := fmt.Sprintf("%v.jpg", i)

	newFile, err := os.Create(filepath.Join(tempDir, imageName))
	if err != nil {
		return newFile, err
	}
	err = jpeg.Encode(newFile, subImg, nil)
	if err != nil {
		return newFile, err
	}
	_, err = newFile.Seek(0, 0)
	if err != nil {
		return newFile, err

	}
	return newFile, nil
}

func (s imageCropper) getImgWithConfig(file *os.File) (image.Image, image.Config, error) {
	logSource := "[imageCropper.getImgWithConfig]"

	var img image.Image
	var imgConfig image.Config

	_, err := file.Seek(0, 0)
	if err != nil {
		return img, imgConfig, fmt.Errorf("%v file.Seek.1 %v", logSource, err)
	}

	img, _, err = image.Decode(file)
	if err != nil {
		return img, imgConfig, fmt.Errorf("%v image.Decode %v", logSource, err)
	}

	_, err = file.Seek(0, 0)
	if err != nil {
		return img, imgConfig, fmt.Errorf("%v file.Seek.2 %v", logSource, err)

	}

	imgConfig, _, err = image.DecodeConfig(file)
	if err != nil {
		return img, imgConfig, fmt.Errorf("%v image.DecodeConfig %v", logSource, err)
	}

	return img, imgConfig, nil
}

func (s imageCropper) deleteUploadedFiles(files []*os.File) {
	for _, file := range files {
		if err := s.fileRepo.DeleteFile(file); err != nil {
			// Pass if some error
			log.Println(err)
		}
	}
}

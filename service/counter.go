package service

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"image_crop/config"
	"time"
)

type (
	CounterService interface {
		Up(key string) (int64, error)
	}

	counterService struct {
		client *redis.Client
	}
)

func NewCounterService(
	cfg config.Config,
	client *redis.Client,
) CounterService {
	return &counterService{
		client: client,
	}
}

func (c *counterService) Up(key string) (int64, error) {
	logSource := "[counterService.Up]"
	value, err := c.client.Incr(context.Background(), key).Result()
	if err != nil {
		return 0, fmt.Errorf("%v %v", logSource, err)
	}
	_, err = c.client.SetEX(context.Background(), key, value, time.Hour * 12).Result()
	return value, nil
}

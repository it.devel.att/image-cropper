package service

import "image"

type (
	CoordinatesService interface {
		GenerateCoordinates(maxWidth, maxHeight, step int) []image.Rectangle
	}

	coordinatesService struct {
	}
)

func NewCoordinatesService() CoordinatesService {
	return coordinatesService{}
}

func (s coordinatesService) GenerateCoordinates(maxWidth, maxHeight, step int) []image.Rectangle {
	oneWidthSize := maxWidth / step
	oneHeightSize := maxHeight / step
	var coordinates []image.Rectangle
	for height := 0; height < maxHeight; height += oneHeightSize {
		for width := 0; width < maxWidth; width += oneWidthSize {
			coordinates = append(coordinates, image.Rect(width, height, width+oneWidthSize, height+oneHeightSize))
		}
	}
	return coordinates
}

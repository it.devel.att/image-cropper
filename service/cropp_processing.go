package service

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"image_crop/repository"
	"os"
	"time"
)

type (
	CropProcessingService interface {
		StartProcessing(file *os.File) error
	}

	cropProcessingService struct {
		cfg               config.Config
		imgCropperService ImageCropper
		taskService       TaskService
		fileRepo          repository.FileRepository
		mapRepo           repository.MapRepository
	}
)

func (s cropProcessingService) StartProcessing(file *os.File) error {
	logSource := "[cropProcessingService.StartProcessing]"
	start := time.Now()
	log.Infof("%v Filename %v", logSource, file.Name())

	if err := s.fileRepo.UploadFile(file, ""); err != nil {
		return fmt.Errorf("%v Error %v", logSource, err)
	}

	newMap, err := s.mapRepo.CreateMap(file.Name())
	if err != nil {
		return fmt.Errorf("%v Error %v", logSource, err)
	}

	//tempDir, err := ioutil.TempDir(s.cfg.GetCroppedImageDir(), "")
	//if err != nil {
	//	return fmt.Errorf("%v Error %v", logSource, err)
	//
	//}

	if err := s.imgCropperService.CreateCropTask(file.Name(), 1, newMap, nil); err != nil {
		return fmt.Errorf("%v%v", logSource, err)
	}

	if err := file.Close(); err != nil {
		log.Print(err)
	}

	elapsed := time.Since(start)
	//log.Printf("%v TempDir is %v", logSource, tempDir)
	//if s.cfg.GetRemoveLocalDirAfterImageCrop() {
	//	if err := os.RemoveAll(tempDir); err != nil {
	//		log.Printf("%v Error %v", logSource, err)
	//	}
	//}

	log.Printf("%v Time took: %v", logSource, elapsed)
	return nil
}

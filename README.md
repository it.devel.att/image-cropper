Install migrate for postgres
```shell script
go get -tags 'postgres' -u github.com/golang-migrate/migrate/v4/cmd/migrate/
```


#### Первая пробная версия с рекурсивным нарезанием карты в один поток
На данный момент дано:
- Картинка: `test_images/earth_map.jpg` 
  - Размер 2.5mb 
  - Разрешение 5200x2600 пикселей
  - Конфигурация нарезания
  ```go
    stepForLevel = map[int]int{
    		1: 2,
    		2: 5,
    		3: 2,
    		4: 10,
    	}
  ```
   - Нарeзается сначала 2x2 
   - Каждый из полученных фрагментов нарезается 5x5 
   - Каждый из полученных фрагментов нарезается 2x2 
   - Каждый из полученных фрагментов нарезается 10x10

- Времени нарезается: `Time took: 13m3.580406721s`
- Всего блоков (map_blocks) получается: `40504`
  - Zoom level 1: 4 блока
  - Zoom level 2: 100 блоков
  - Zoom level 3: 400 блоков
  - Zoom level 4: 40000 блоков

### Будем распаралеливать -->
Всё это тестируется на Lenovo-IdeaPad-S340-15IWL
- Intel Core i5-8265U 1.60 ГГц
- 20GB RAM
- 256GB SSD

Сначала зашёл в тупиковую ситуацию с каналами и рекурсией

- Картинка загружается на разрезку
- Отправляется в канал чтобы воркер подхватил задачу
- Воркер берёт задачу
- Режет картинку и на каждую часть шлёт в этот же канал задачу на разрезание этой части
- В итоге буфер в канале может переполниться, и запись в канал заблокируется, но в таком случае и воркер не может взять задачу из канала и всё встаёт ;)
- Переделал всё на очень простую реализацию thread-safe очереди
```go
func (q *queue) Push(task *model.Task) {
	q.mu.Lock()
	defer q.mu.Unlock()

	q.tasks = append(q.tasks, task)
	q.wg.Add(1)
}

func (q *queue) Get() *model.Task {
	q.mu.Lock()
	defer q.mu.Unlock()
	if len(q.tasks) == 0 {
		return nil
	}

	task := q.tasks[0]
	q.tasks = q.tasks[1:]
	q.wg.Done()
	return task
}

func (q *queue) WaitForProcessAllTasks() {
	logSource := "[queue.WaitForProcessAllTasks]"
	log.Infof("%v Start waiting", logSource)
	q.wg.Wait()
	log.Infof("%v Finish waiting", logSource)

}
```
- WaitGroup нужен чтобы при завершении работы приложения все таски из очереди были взяты и обработаны
- Поэтому сначала ждём когда воркеры всё вычтут и обработают из очереди, и только потом останавливаем воркеры
- Но тут возникает опять же, небольшая проблема, воркеры при обработке задачи рекурсивно пушат в эту же очередь задачи на разрезание разрезанных картинок в следствии чего приложение не остановится пока полностью не закончит начатую обработку

И ещё хотелось бы как-то точно подсчитать сколько всего времени ушло на обработку картинки для её полного нарезания, а задачи асинхронны

- Как вариант это после обработки каждой задачи опрашивать базу сколько частей обработано и сравнивать с тем сколько частей должно быть всего, и если всё сошлось то вставлять время завершения
- Но это дополнительные `40504` запросов в базу на каждую задачу
- Возможно счётчик редиса поможет
> Счётчик редиса считал как-то очень неправильно...

В итоге пришлось всё-таки делать опрос базы на кол-во подсчитанных блоков каждый при завершении таски

100 воркеров процессинг всё той же картинки занимает 1m51s
```go
Finish at: 1m51.823036s
```
200 воркеров
```go
Finished at: 1m32.56179s
```
> Но начинают появляться ошибки
```go
[worker.Run][imageCropper.CropByTask.4][fileRepository.UploadFile] Error: RequestError: send request failed
caused by: Put "http://localhost:9002/demo-bucket/cropped_images/493094830/15.jpg": read tcp 127.0.0.1:33346->127.0.0.1:9002: read: connection reset by peer
```
> Возможно можно как-то подкрутить minio на большое кол-во подключений

300 воркеров
> Появляется больше ошибок 
```go
[worker.Run][imageCropper.CropByTask.4] Error open cropped_images/958074681/10.jpg: too many open files
[worker.Run][imageCropper.CropByTask.4][fileRepository.UploadFile] Error: RequestError: send request failed caused by: Put "http://localhost:9002/demo-bucket/cropped_images/596246988/3.jpg": dial tcp: lookup localhost: device or resource busy
[worker.Run][imageCropper.CropByTask.3][mapRepository.CreateMapBlock] Error ERROR: invalid input syntax for type uuid: "" (SQLSTATE 22P02)
[worker.Run][imageCropper.CropByTask.4]RequestError: send request failed caused by: Get "http://localhost:9002/demo-bucket/cropped_images/024444458/2.jpg": dial tcp 127.0.0.1:9002: socket: too many open files
```
Их в логах очень много в следствии чего процессинг даже не может адекватно завершиться

На данный момент самая стабильная обработка это в 100-200 воркеров на одном инстансе

Результат с 13m3.580406721s до 1m32.56179s
> Для одновременно одной обрабатываемой картинки

Всё-таки если одновременно резать несколько картинок на такое кол-во то каждая новая кратинка будет обрабатываться дольше вследствии ограниченного числа воркеров

### Пора масштабировать очередь обработки картинок -->


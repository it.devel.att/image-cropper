package model

import (
	"time"
)

type Map struct {
	ID                string     `db:"id" json:"id"`
	OriginalImage     string     `db:"original_image" json:"original_image"`
	CreatedAt         time.Time  `db:"created_at" json:"created_at"`
	FinishProcessedAt *time.Time `db:"finish_processed_at" json:"finish_processed_at"`
}

type MapBlock struct {
	ID               string    `db:"id" json:"id"`
	MapID            string    `db:"map_id" json:"map_id"`
	ParentMapBlockID *string   `db:"parent_map_block_id" json:"parent_map_block_id"`
	BlockOrder       int       `db:"block_order" json:"block_order"`
	Image            string    `db:"image" json:"image"`
	ZoomLevel        int       `db:"zoom_level" json:"zoom_level"`
	CreatedAt        time.Time `db:"created_at" json:"created_at"`
}

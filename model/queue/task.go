package queue

import (
	"fmt"
	model "image_crop/model/db"
)

type Task struct {
	FileKey     string          `json:"file_key"`
	Level       int             `json:"level"`
	NewMap      *model.Map      `json:"new_map"`
	ParentBlock *model.MapBlock `json:"parent_block"`
}

func (t Task) GetUPKey() string {
	// TODO Handle NewMap can be nil?
	return fmt.Sprintf("map:%v:processed_blocks", t.NewMap.ID)
}

package metric

import (
	"context"
	"errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"net/http"
	"time"
)

type (
	Server struct {
		cfg        config.Config
		httpServer *http.Server
	}
)

func NewMetricServer(cfg config.Config) *Server {
	http.Handle("/metrics", promhttp.Handler())

	return &Server{
		httpServer: &http.Server{
			Addr:    cfg.GetMetricPort(),
			Handler: nil,
		},
	}
}

func (s *Server) StartServer() {
	const logSource = "[metric.Server.StartServer]"

	log.Infof("%v Start server", logSource)
	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("%v Fail to start metric server: %v", logSource, err)
		}
	}()
}

func (s *Server) StopServer() {
	const logSource = "[metric.Server.StopServer]"

	// TODO Set timeout from config
	ctxWithTimeOut, timeOutCancelFunc := context.WithTimeout(context.Background(), time.Second*5)
	defer timeOutCancelFunc()

	log.Infof("%v Shutdown http server", logSource)
	if err := s.httpServer.Shutdown(ctxWithTimeOut); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Errorf("%v Error while try to shutdown server: %v", logSource, err)
	}
}

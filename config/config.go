package config

import (
	"github.com/caarlos0/env/v6"
	"log"
)

type (
	Config interface {
		GetDBConnectionString() string
		GetS3AccessKey() string
		GetS3SecretKey() string
		GetS3EndpointURL() string
		GetS3BucketName() string
		GetCroppedImageDir() string
		GetRemoveLocalDirAfterImageCrop() bool
		GetMetricPort() string
		GetServerPort() string
		GetWorkerPoolSize() int
		GetRedisURL() string
		GetLogLevel() string
	}

	config struct {
		DBConnectionString           string `env:"DB_CONNECTION_STRING" envDefault:"postgres://root:root@localhost:5432/image_cropper?sslmode=disable"`
		S3AccessKey                  string `env:"S3_ACCESS_KEY" envDefault:"access_key"`
		S3SecretKey                  string `env:"S3_SECRET_KEY" envDefault:"secret_key"`
		S3EndpointURL                string `env:"S3_ENDPOINT_URL" envDefault:"http://localhost:9002"`
		S3BucketName                 string `env:"S3_BUCKET_NAME" envDefault:"demo-bucket"`
		CroppedImageDir              string `env:"CROPPED_IMAGE_DIR"`
		RemoveLocalDirAfterImageCrop bool   `env:"REMOVE_LOCAL_DIR_AFTER_IMAGE_CROP" envDefault:"true"`
		MetricPort                   string `env:"METRIC_PORT" envDefault:":2323"`
		ServerPort                   string `env:"SERVER_PORT" envDefault:":2000"`
		WorkerPoolSize               int    `env:"WORKER_POOL_SIZE" envDefault:"5"`
		RedisURL                     string `env:"REDIS_URL" envDefault:"redis://localhost:6379/0"`
		LogLevel                     string `env:"LOG_LEVEL" envDefault:"info"`
	}
)

func NewConfig() Config {
	cfg := &config{}
	if err := env.Parse(cfg); err != nil {
		log.Fatal(err)
	}
	return cfg
}

func (c config) GetDBConnectionString() string {
	return c.DBConnectionString
}

func (c config) GetS3AccessKey() string {
	return c.S3AccessKey
}

func (c config) GetS3SecretKey() string {
	return c.S3SecretKey
}

func (c config) GetS3EndpointURL() string {
	return c.S3EndpointURL
}

func (c config) GetS3BucketName() string {
	return c.S3BucketName
}

func (c config) GetCroppedImageDir() string {
	return c.CroppedImageDir
}

func (c config) GetRemoveLocalDirAfterImageCrop() bool {
	return c.RemoveLocalDirAfterImageCrop
}

func (c config) GetMetricPort() string {
	return c.MetricPort
}

func (c config) GetServerPort() string {
	return c.ServerPort
}

func (c config) GetWorkerPoolSize() int {
	return c.WorkerPoolSize
}

func (c config) GetRedisURL() string {
	return c.RedisURL
}

func (c config) GetLogLevel() string {
	return c.LogLevel
}

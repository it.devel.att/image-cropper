package server

import "github.com/gin-contrib/cors"

func (s *Server) initRoutes() {
	s.router.Use(cors.Default())


	s.router.POST("/api/v1/maps/", s.UploadNewMap)
	s.router.GET("/api/v1/maps/", s.GetMaps)
	s.router.GET("/api/v1/maps/:id", s.GetMapBlocks)
	s.router.GET("/api/v1/map-block/:id", s.GetMapBlocksByBlock)
}

package server

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"image_crop/repository"
	"image_crop/service"
	"net/http"
	"time"
)

type Server struct {
	cfg            config.Config
	cropperService service.ImageCropper
	fileRepo       repository.FileRepository
	mapRepo        repository.MapRepository
	router         *gin.Engine
	httpServer     *http.Server
}

func NewServer(
	cfg config.Config,
	cropperService service.ImageCropper,
	fileRepo repository.FileRepository,
	mapRepo repository.MapRepository,
) *Server {
	return &Server{
		cfg:            cfg,
		cropperService: cropperService,
		fileRepo:       fileRepo,
		mapRepo:        mapRepo,
		router:         gin.Default(),
	}
}

func (s *Server) initHTTPServer() {
	s.httpServer = &http.Server{
		Addr:    s.cfg.GetServerPort(),
		Handler: s.router,
	}
}

func (s *Server) StartServer() {
	const logSource = "[Server.StartServer]"
	s.initHTTPServer()
	s.initRoutes()

	log.Infof("%v Start server", logSource)
	go func() {
		if err := s.httpServer.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("%v ListenAndServe error: %v", logSource, err)
		}
	}()
}

func (s *Server) StopServer() {
	const logSource = "[Server.StopServer]"

	// TODO Set timeout from config
	ctxWithTimeOut, timeOutCancelFunc := context.WithTimeout(context.Background(), time.Second*5)
	defer timeOutCancelFunc()

	log.Infof("%v Shutdown http server", logSource)
	if err := s.httpServer.Shutdown(ctxWithTimeOut); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Errorf("%v Error while try to shutdown server: %v", logSource, err)
	}
}

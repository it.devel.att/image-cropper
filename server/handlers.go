package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	model "image_crop/model/db"
	"io/ioutil"
	"net/http"
)

func (s Server) UploadNewMap(c *gin.Context) {
	logSource := "[Server.UploadNewMap]"
	fmt.Println("File Upload Endpoint Hit")
	file, err := c.FormFile("map")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "No file is received",
		})
		return
	}
	tempFile, err := ioutil.TempFile("", fmt.Sprintf("*-%v", file.Filename))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to create tempfile",
		})
		return
	}
	log.Infof("%v%v", logSource, tempFile.Name())
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	openFile, err := file.Open()
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to open file",
		})
		return
	}
	fileBytes, err := ioutil.ReadAll(openFile)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to read file",
		})
		return
	}
	// write this byte array to our temporary file
	if _, err := tempFile.Write(fileBytes); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to read file",
		})
		return
	}
	newMap, err := s.cropperService.CropImage(tempFile)
	if err != nil {
		log.Errorf("%v%v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to crop image",
		})
		return
	}
	// return that we have successfully uploaded our file!
	c.JSON(http.StatusOK, newMap)
	return
}

func (s Server) GetMaps(c *gin.Context) {
	logSource := "[Server.GetMaps]"

	maps, err := s.mapRepo.GetMaps()
	if err != nil {
		log.Errorf("%v%v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get maps",
		})
		return
	}
	for _, item := range maps {
		url, err := s.fileRepo.GetSignURL(item.OriginalImage)
		if err == nil {
			item.OriginalImage = url
		}
	}
	c.JSON(http.StatusOK, maps)
	return
}

func (s Server) GetMapBlocks(c *gin.Context) {
	logSource := "[Server.GetMapBlocks]"

	mapID := c.Param("id")
	if mapID == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get maps",
		})
		return
	}

	existMap, err := s.mapRepo.GetMap(mapID)
	if err != nil {
		log.Errorf("%v Error: %v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get map",
		})
		return
	}
	blocks, err := s.mapRepo.GetMapBlocksByMap(existMap)
	if err != nil {
		log.Errorf("%v Error: %v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get map blocks",
		})
		return
	}
	blocksRows := make([][]*model.MapBlock, 2)
	count := 0
	for _, item := range blocks {
		if count < 2 {
			blocksRows[0] = append(blocksRows[0], item)
		} else {
			blocksRows[1] = append(blocksRows[1], item)
		}
		url, err := s.fileRepo.GetSignURL(item.Image)
		if err == nil {
			item.Image = url
		}
		count++
	}
	c.JSON(http.StatusOK, blocksRows)
	return
}

func (s Server) GetMapBlocksByBlock(c *gin.Context) {
	logSource := "[Server.GetMapBlocks]"

	mapID := c.Param("id")
	if mapID == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get maps",
		})
		return
	}

	existMapBlock, err := s.mapRepo.GetMapBlock(mapID)
	if err != nil {
		log.Errorf("%v Error: %v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get map",
		})
		return
	}
	blocks, err := s.mapRepo.GetChildMapBlocks(existMapBlock)
	if err != nil {
		log.Errorf("%v Error: %v", logSource, err)
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"message": "Error while try to get child map blocks",
		})
		return
	}
	for _, item := range blocks {
		url, err := s.fileRepo.GetSignURL(item.Image)
		if err == nil {
			item.Image = url
		}
	}
	if len(blocks) > 0 {
		zoomLevel := blocks[0].ZoomLevel

		rowsOnZoomLevel := map[int]int{
			1: 2,
			2: 5,
			3: 2,
			4: 10,
		}

		blocksRows := make([][]*model.MapBlock, rowsOnZoomLevel[zoomLevel])
		count := 0
		for i := 0; i < rowsOnZoomLevel[zoomLevel]*rowsOnZoomLevel[zoomLevel]; i += rowsOnZoomLevel[zoomLevel] {
			blocksRows[count] = blocks[i : i+rowsOnZoomLevel[zoomLevel]]
			count++
		}
		c.JSON(http.StatusOK, blocksRows)
		return
	}
	c.JSON(http.StatusOK, nil)
	return
}

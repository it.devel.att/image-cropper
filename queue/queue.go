package queue

import (
	log "github.com/sirupsen/logrus"
	model "image_crop/model/queue"
	"sync"
)

type (
	Queue interface {
		Push(task *model.Task)
		Get() *model.Task
		WaitForProcessAllTasks()
	}
	queue struct {
		wg    *sync.WaitGroup
		mu    *sync.Mutex
		tasks []*model.Task
	}
)

func NewQueue() Queue {
	return &queue{
		wg: &sync.WaitGroup{},
		mu: &sync.Mutex{},
	}
}

func (q *queue) Push(task *model.Task) {
	q.mu.Lock()
	defer q.mu.Unlock()

	q.tasks = append(q.tasks, task)
	q.wg.Add(1)
}

func (q *queue) Get() *model.Task {
	q.mu.Lock()
	defer q.mu.Unlock()
	if len(q.tasks) == 0 {
		return nil
	}

	task := q.tasks[0]
	q.tasks = q.tasks[1:]
	q.wg.Done()
	return task
}

func (q *queue) WaitForProcessAllTasks() {
	logSource := "[queue.WaitForProcessAllTasks]"
	log.Infof("%v Start waiting", logSource)
	q.wg.Wait()
	log.Infof("%v Finish waiting", logSource)

}

package db

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"image_crop/config"
	"log"
)

func NewS3Storage(cfg config.Config) *s3.S3 {
	s3Config := &aws.Config{
		Credentials:      credentials.NewStaticCredentials(
			cfg.GetS3AccessKey(),
			cfg.GetS3SecretKey(),
			"",
			),
		Endpoint:         aws.String(cfg.GetS3EndpointURL()),
		Region:           aws.String("us-east-1"),
		DisableSSL:       aws.Bool(true),
		S3ForcePathStyle: aws.Bool(true),
	}
	newSession, err := session.NewSession(s3Config)
	if err != nil {
		log.Fatalf("Suka")
	}
	return s3.New(newSession)
}

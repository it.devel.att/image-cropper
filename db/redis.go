package db

import (
	"github.com/go-redis/redis/v8"
	"image_crop/config"
	"log"
)

func NewRedisClient(
	cfg config.Config,
) *redis.Client {
	const logSource = "[NewRedisClient]"

	redisCfg, err := redis.ParseURL(cfg.GetRedisURL())
	if err != nil {
		log.Fatalf("%v Error: %v", logSource, err)
	}
	client := redis.NewClient(redisCfg)
	return client
}

ALTER TABLE maps
    ADD COLUMN IF NOT EXISTS finish_processed_at timestamp NULL;
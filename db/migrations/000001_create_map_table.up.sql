CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE maps
(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    original_image TEXT                    NOT NULL,
    created_at     timestamp DEFAULT now() NOT NULL
)
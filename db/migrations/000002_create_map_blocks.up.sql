CREATE TABLE map_blocks
(
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4() NOT NULL,
    map_id UUID REFERENCES maps (id) ON DELETE CASCADE NOT NULL,
    parent_map_block_id UUID REFERENCES map_blocks (id) ON DELETE CASCADE NULL,
    block_order INT                     NOT NULL,
    image       TEXT                    NOT NULL,
    zoom_level  INT                     NOT NULL,
    created_at  timestamp DEFAULT now() NOT NULL
)
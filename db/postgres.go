package db

import (
	_ "github.com/jackc/pgx"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"time"
)

func NewPostgresDB(
	cfg config.Config,
) *sqlx.DB {
	db, err := sqlx.Connect("pgx", cfg.GetDBConnectionString())
	if err != nil {
		log.Fatalf("[NewPostgresDB] Error: %v", err)
	}

	// TODO set it from config
	db.SetConnMaxLifetime(time.Minute)
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(100)
	return db
}

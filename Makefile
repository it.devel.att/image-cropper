MIGRATION_FORMAT=sql
MIGRATION_DIR_SOURCE=db/migrations
DATABASE_DSN=postgres://root:root@localhost:5432/image_cropper?sslmode=disable
DOCKER_REGISTRY=itdevelatt# Use your own
DOCKER_IMAGE_NAME=image-crop-microservice# Use your own
APP_SERVICE_NAME=image-crop-microservice
DOCKER_IMAGE=${DOCKER_REGISTRY}/${DOCKER_IMAGE_NAME}

env:
	cp .env.example .env

up_postgres:
	docker-compose up -d postgres

up_minio:
	docker-compose up -d minio

up_compose_local:
	TAG=dev docker-compose up -d

fmt:
	go fmt ./...

vet:
	go vet ./...

test:
	go test ./... -v

local-test: up_postgres up_minio test

create_migration:
	migrate create -ext ${MIGRATION_FORMAT} -dir ${MIGRATION_DIR_SOURCE} -seq $(NAME)

migrate_up:
	migrate -database "${DATABASE_DSN}" -path ${MIGRATION_DIR_SOURCE} up $(COUNT)

migrate_down:
	migrate -database "${DATABASE_DSN}" -path ${MIGRATION_DIR_SOURCE} down $(COUNT)

migrate_drop_all:
	migrate -database "${DATABASE_DSN}" -path ${MIGRATION_DIR_SOURCE} drop -f

help:
	@echo "TODO add help info here"

docker_build:
	docker build . -t ${DOCKER_IMAGE}:$(TAG)

docker_push:
	docker push ${DOCKER_IMAGE}:$(TAG)
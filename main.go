package main

import (
	"context"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"image_crop/config"
	"image_crop/db"
	"image_crop/metric"
	"image_crop/queue"
	"image_crop/repository"
	"image_crop/server"
	"image_crop/service"
	"image_crop/worker"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	logSource := "[main.init]"

	if _, err := os.Stat(".env"); err == nil {
		if err := godotenv.Load(); err != nil {
			log.Infof("%v Error while try to godotenv.Load: %v", logSource, err)
		}
	} else {
		log.Infof("%v File .env not exists. Pass to load env variables from file", logSource)
	}
}

func setLogLevel(cfg config.Config) {
	level, err := log.ParseLevel(cfg.GetLogLevel())
	if err != nil {
		log.Errorf("[main.setLogLevel] Error: %w", err)
	} else {
		log.SetLevel(level)
	}
}

func main() {
	cfg := config.NewConfig()
	setLogLevel(cfg)

	s3Client := db.NewS3Storage(cfg)
	pgClient := db.NewPostgresDB(cfg)
	redisClient := db.NewRedisClient(cfg)

	fileRepo := repository.NewFileRepository(cfg, s3Client)
	mapRepo := repository.NewMapRepository(cfg, pgClient)

	taskQueue := queue.NewQueue()

	counterService := service.NewCounterService(cfg, redisClient)
	taskService := service.NewTaskService(cfg, taskQueue)
	coordService := service.NewCoordinatesService()
	cropperService := service.NewImageCropper(cfg, fileRepo, mapRepo, taskService, coordService, counterService)

	srv := server.NewServer(cfg, cropperService, fileRepo, mapRepo)
	metricServer := metric.NewMetricServer(cfg)

	ctx, cancelFun := context.WithCancel(context.Background())
	workerPool := worker.NewWorkerPool(
		cfg,
		ctx,
		taskQueue,
		cropperService,
		taskService,
		counterService,
		mapRepo,
	)

	srv.StartServer()
	metricServer.StartServer()
	workerPool.Run()

	// For graceful shutdown
	termChan := make(chan os.Signal)
	signal.Notify(termChan, syscall.SIGINT, syscall.SIGTERM)
	<-termChan

	start := time.Now()

	srv.StopServer()
	metricServer.StopServer()
	log.Infof("[main] Receive interrupt signal. Close server and wait for worker to stop")

	// First wait when queue is be empty
	taskQueue.WaitForProcessAllTasks()

	cancelFun()

	// Only after it stop workers!
	workerPool.WaitForFinish()

	end := time.Since(start)

	log.Infof("[main] Graceful shutdown took: %v", end)
	//wg := &sync.WaitGroup{}
	//for i := 0; i < 40000; i++ {
	//	wg.Add(1)
	//
	//	go func() {
	//		defer wg.Done()
	//		redisClient.Incr(context.Background(), "hello")
	//	}()
	//}
	//wg.Wait()

}
